function wpCustomSearchCPT(){
	this.$ = jQuery; 
	this.searchfilterscontainer = '.customsearch-cpt-container .search-filters-wrapper .filter-wrapper';
	this.dropdownCount = 0;
	this.visibleDropdownCount = 0;
	this.dropdownHTML = {};
	this.currentPage = 0;
	this.postsPerPage = 0;
	this.pagedResults = {};
	this.defaultTaxonomy = false;
	this.filteredResults = [];
};
wpCustomSearchCPT.prototype.generateDropdownHTML = function(filterInfo){
	var $ = this.$;
	this.dropdownCount++;
	
	var downArrowIMG = $(".customsearch-cpt-container #hidden-arrow").attr('src');
	
	var tmpHTML = '<div data-taxonomy="'+filterInfo.tax_key+'" id="dd-'+this.dropdownCount+'" class="wrapper-dropdown-searchcpt noselection" tabindex="1"><span class="dd-value">'+filterInfo.tax_label+'</span>';
	tmpHTML +='<ul class="searchcpt-dropdown">';
	filterInfo.tax_values.sort();
	for(var i = 0; i < filterInfo.tax_values.length; i++){
		var currTaxValue = filterInfo.tax_values[i];
		tmpHTML += '<li><a class="searchcpt-dropdown-option noshow" data-taxonomy="'+filterInfo.tax_key+'" data-taxvalue="'+currTaxValue+'" value="'+currTaxValue+'">'+currTaxValue+'</a></li>';
	}
	tmpHTML += '</ul><img class="dropdown-arrow" src="'+downArrowIMG+'" /></div>';
	this.dropdownHTML[filterInfo.tax_key] = {};
	this.dropdownHTML[filterInfo.tax_key]['html']=tmpHTML;
	this.dropdownHTML[filterInfo.tax_key]['dom_id']='dd-'+this.dropdownCount;
	this.dropdownHTML[filterInfo.tax_key]['label'] = filterInfo.tax_label;
	return tmpHTML;
};
wpCustomSearchCPT.prototype.populateSearchFilters = function(tax_key){
	var $ = this.$;
	var app = this;
	var tmpHTML = '';
	var taxValues = [];
	if(typeof tax_key === 'undefined'){
		for (var key in customsearchcpt_config['CPT']['Filtering']){
			var filterObj = customsearchcpt_config['CPT']['Filtering'][key];
			if(this.visibleDropdownCount == 0){
				this.defaultTaxonomy = filterObj.tax_key; //use first as default
			}
			tmpHTML += this.dropdownHTML[filterObj.tax_key]['html'];
		}
	}else{
			//Got Each Taxonomy
			var key2check = $( ".customsearch-cpt-container .wrapper-dropdown-searchcpt.selected" ).last().data('taxonomy');//$( ".wrapper-dropdown-searchcpt.selected" ).last().data('taxonomy');
			var val2check = $(".customsearch-cpt-container [data-taxonomy='"+key2check+"']").find('.dd-value').text();
			
			if(key2check != '' && val2check != ''){
				
				
					
				$.each(customsearchcpt_posts,function(jj,oo){
						if(oo[key2check] == val2check){
							if( $.inArray(oo[tax_key] , taxValues) == -1 ){
								taxValues.push(oo[tax_key] );
							}
						}
				});
				
				
			
			}
			
		
	}
	
	$(this.searchfilterscontainer).html($(app.searchfilterscontainer).html() + tmpHTML,function(){
		if(app.visibleDropdownCount == 0){
			app.visibleDropdownCount++;
			var tmpTax = customsearchcpt_config['CPT']['Filtering'][app.visibleDropdownCount]['tax_key'];
			$(".customsearch-cpt-container [data-taxonomy]").removeClass('noshow').addClass('noshow');
			$(".customsearch-cpt-container [data-taxonomy='"+tmpTax+"'].wrapper-dropdown-searchcpt").addClass('noshow').removeClass('noshow');
			$(".customsearch-cpt-container [data-taxonomy='"+tmpTax+"'] .searchcpt-dropdown-option").addClass('noshow').removeClass('noshow');
		}else{
			var tmpFilterObj = customsearchcpt_config['CPT']['Filtering'][String(app.visibleDropdownCount - 1)];
			var conditionRaw = String(tmpFilterObj.condition).split('==');
			//'condition'=>'advisor_state==FL',
			var tmpTaxKey = conditionRaw[0];
			var tmpTaxVal = conditionRaw[1];
			if(tmpTaxKey && tmpTaxVal){
				if($(".customsearch-cpt-container [data-taxonomy='"+tmpTaxKey+"']").find('.dd-value').text() != tmpTaxVal){
					
					if(customsearchcpt_config['CPT']['Filtering'][app.visibleDropdownCount]['tax_key'] == tmpFilterObj.replace_key){
						$(".customsearch-cpt-container [data-taxonomy='"+tmpFilterObj.replace_key+"'].wrapper-dropdown-searchcpt").addClass('noshow');
						app.addNextFilter();
						return true;
					}
					
				}else{
					$(".customsearch-cpt-container [data-taxonomy='"+tmpFilterObj.tax_key+"'].wrapper-dropdown-searchcpt").addClass('noshow');
					app.addNextFilter();
					return true;
				}
			}else{
				
				$(".customsearch-cpt-container [data-taxonomy='"+tax_key+"'] .searchcpt-dropdown-option").addClass('noshow');
				$(".customsearch-cpt-container [data-taxonomy='"+tax_key+"'].wrapper-dropdown-searchcpt").removeClass('noshow');
				
			}
			
			for(var i = 0; i < taxValues.length; i++){
				$(".customsearch-cpt-container [data-taxvalue='"+taxValues[i]+"']").removeClass('noshow');
			}
			
		}
        
        
        
        /* HOT FIX for Advisor Login-Support */
        var isSupport = false;
        if($('[data-is_support]').data('is_support') == "yes"){
            isSupport = true;
        }
        var real_href = $('[data-taxonomy="advisor_portal"] .searchcpt-dropdown-option').not('.noshow').attr('value');
        var phoneNumber = '';
        console.log('real_href',real_href);
        
        if(String(real_href).indexOf('bencor.rprgonline.com') !== -1){
            phoneNumber = "1-866-296-9712";
        }else if(String(real_href).indexOf('mybencor.trsretire.com') !== -1){
            phoneNumber = "(888) 258-3422";
        }else if(String(real_href).indexOf('secure.transamerica.com') !== -1){
            phoneNumber = "(888) 258-3422";
        }
        console.log('phone',phoneNumber);
        console.log('isSupport',isSupport);
        
        if(isSupport && phoneNumber != ''){
            $("#login-support-result").remove();
            $('.search-filters-wrapper').append($('<div id="login-support-result" style="clear:both;padding-top:2em;"><div class="columns medium-10">\
                        <h3>BENCOR</h3>\
                        <p id="login-support-phone">Participant Support: '+phoneNumber+' </p>\
                        </div></div>'));
        }
	});
	
	
};
wpCustomSearchCPT.prototype.addNextFilter = function(){
	var $ = this.$;
	var filter_tax_key = '';
	var filterObj = customsearchcpt_config['CPT']['Filtering'][String(this.visibleDropdownCount + 1)];
	if(typeof filterObj !== 'undefined'){
		//check exceptions
		if(typeof filterObj.condition !== 'undefined'){
			var conditionRaw = String(filterObj.condition).split('==');
			//'condition'=>'advisor_state==FL',
			var tmpTaxKey = conditionRaw[0];
			var tmpTaxVal = conditionRaw[1];
			if($(".customsearch-cpt-container [data-taxonomy='"+tmpTaxKey+"']").find('.dd-value').text() == tmpTaxVal){
				filter_tax_key = filterObj.replace_key;
			}else{
				filter_tax_key = filterObj.tax_key;
			}
		}else{
			filter_tax_key = filterObj.tax_key;
		}
		this.visibleDropdownCount++;
		this.populateSearchFilters(filter_tax_key);
	}
    
};
wpCustomSearchCPT.prototype.initPagination = function(results){
	var $ = this.$;
	
	if(this.postsPerPage == 0){
		this.postsPerPage = parseInt(customsearchcpt_config.CPT.Pagination.posts_per_page,10);
	}
	if(this.currentPage == 0){
		this.currentPage++;
	}	
	
	var pages = Math.ceil( results.length/this.postsPerPage );
	this.pagedResults = {'length':pages};
	for(var i = 1; i <= pages; i++){
		this.pagedResults[String(i)] = [];
	}

	console.log('pagedResults',this.pagedResults);
	
	var tmpPageCount = 1;
	var tmpPostCount = 0;
	var postsPerPage = this.postsPerPage;
	var pagedResults = this.pagedResults;
	$.each(results,function(i,obj){
		console.log('results',results,tmpPageCount);
		
		pagedResults[String(tmpPageCount)].push(obj);
		tmpPostCount++;
		if(tmpPostCount == postsPerPage){
			tmpPostCount = 0;
			tmpPageCount++;
		}
		
	});
	this.pagedResults = pagedResults;

	if(this.currentPage == 1){
		return this.pagedResults[String(this.currentPage)];
	}
	return false;
};
wpCustomSearchCPT.prototype.filterResults = function(){
	var $ = this.$;
	
	var searchFor = $('.customsearch-cpt-container .search-text-box').val();
	var firstTaxValue = $(".customsearch-cpt-container #dd-1.selected").find('.dd-value').text();
	var filteredResults = this.filteredResults;
	if(!searchFor && !firstTaxValue)
		console.log('nothing to search for buddy...');

	if(searchFor){
		//hide other stuff
		$(".customsearch-cpt-container .filter-wrapper").hide();
	}
	$(".customsearch-cpt-container #customsearch-results-container").show();
	$('html,body').animate({
	    scrollTop: $(".customsearch-cpt-container #customsearch-results-container").offset().top
	  }, 500);
	$(".customsearch-cpt-container #customsearchcpt-table").removeClass('noshow');
	//start spitting HTML
	var taxKeys = [];
	var taxValues = [];
	
	$.each($(".customsearch-cpt-container .wrapper-dropdown-searchcpt.selected"),function(j,taxSelection){
		var tmpTaxKey = $(taxSelection).data('taxonomy') ;
		taxKeys.push(tmpTaxKey);
		var tmpTaxVal = $(".customsearch-cpt-container [data-taxonomy='"+tmpTaxKey+"']").find('.dd-value').text();
		if( $.inArray(tmpTaxVal, taxValues) == -1 ){
			taxValues.push(tmpTaxVal);
		}
	});
	
	var tmpHTML = '';
	
	if(filteredResults.length == 0)
		filteredResults = customsearchcpt_posts;
	
	var newFilteredResults = [];
	$.each(filteredResults,function(i,obj){
		
		var key2check = $( ".customsearch-cpt-container .wrapper-dropdown-searchcpt.selected" ).last().data('taxonomy');//$( ".wrapper-dropdown-searchcpt.selected" ).last().data('taxonomy');
		var val2check = $(".customsearch-cpt-container [data-taxonomy='"+key2check+"']").find('.dd-value').text();
		if(searchFor != ''){
			val2check = String(searchFor).toLowerCase();
			for(var x = 0; x < customsearchcpt_metadata.length; x++){
				var tmpTax = customsearchcpt_metadata[x];
				var tmpValue = String(obj[tmpTax]).toLowerCase();
				
				if(String(tmpValue).indexOf(val2check) > -1){
					newFilteredResults.push(obj);
					break;
				}
			}
		}else if(key2check != '' && val2check != ''){
			if(typeof obj[key2check] !== 'undefined'){
				if(obj[key2check] == val2check){
					newFilteredResults.push(obj);
				}
			}
		}
		
	});
	this.filteredResults = newFilteredResults;	
	
	if(typeof this.pagedResults.length === 'undefined'){
		//no paging yet - set paging!
		this.initPagination(this.filteredResults);
	}
	this.populatePaginationButtons();
	this.showResultsPage(this.currentPage);
	
};
wpCustomSearchCPT.prototype.populatePaginationButtons = function(){
	var $ = this.$;
	
	$(".customsearch-cpt-container .custom-search-pagination.pages").html('');
	var tmpHTML = '';
	for(var i = 1; i <= this.pagedResults.length; i++){
		tmpHTML += '<li><a data-gotopage="'+i+'">'+i+'</a></li>';
	}
	$(".customsearch-cpt-container .custom-search-pagination.pages").html(tmpHTML);
};
wpCustomSearchCPT.prototype.showResultsPage = function(i){
	var $ = this.$;
	$(".customsearch-cpt-container .customsearchcpt-tbl-row").hide();
	console.log('i',i);
	if(typeof this.pagedResults[String(i)] !== 'undefined'){
		$(".customsearch-cpt-container #empty-results-msg").html('');
		$.each(this.pagedResults[String(i)],function(j,obj){
			$(".customsearch-cpt-container #customsearchcpt-table-row-"+obj['post_id']).show();
		});
	}else{
		console.log('failed to paginate results!',this.filteredResults);
		$(".customsearch-cpt-container #empty-results-msg").html( customsearchcpt_config.CPT.Pagination.empty_results_msg );
	}
	$(".customsearch-cpt-container [data-gotopage]").removeClass('active');
	$(".customsearch-cpt-container [data-gotopage='"+i+"']").addClass('active');
	
};
wpCustomSearchCPT.prototype.get = function(what){
	if(typeof this[what] !== 'undefined'){
		return this[what];
	}
};
wpCustomSearchCPT.prototype.init = function(){
	var $ = this.$;
	
	for (var taxLabel in customsearchcpt_taxonomy) {
		var tmpTaxObj = customsearchcpt_taxonomy[taxLabel];
		this.generateDropdownHTML(tmpTaxObj);
	}
	$(".customsearch-cpt-container #customsearchcpt-table").removeClass('noshow').show();
};
wpCustomSearchCPT.prototype.resetSearchForm = function(){
	var $ = this.$;
	this.currentPage = 0;
	this.pagedResults = {};
	this.filteredResults = [];
    /* HOT FIX for Advisor Login-Support */
    var isSupport = false;
    if($('[data-is_support]').data('is_support') == "yes"){
        isSupport = true;
    }
    if(isSupport){
        $("#login-support-result").remove();
    }
	$(".customsearch-cpt-container #customsearch-results-container").hide();
	$(this.searchfilterscontainer).html('');
	this.visibleDropdownCount=0;
	$(".customsearch-cpt-container .filter-wrapper").show();
	$(".customsearch-cpt-container #customsearchcpt-table").removeClass('nowshow').addClass('noshow');
	this.filteredResults = [];
	$('.customsearch-cpt-container img.searchicon-img').removeClass('disabled');
	$('.customsearch-cpt-container .search-text-box').val('').removeAttr('disabled');
	this.populateSearchFilters();
};
wpCustomSearchCPT.prototype.pageCTRL = function(action){
	console.log('action',action);
	switch(action){
	case "next":
		if(typeof this.pagedResults[String(this.currentPage + 1)] !== 'undefined'){
			this.currentPage++;
			this.showResultsPage(this.currentPage);
		}
		break;
	case "previous":
		if(typeof this.pagedResults[String(this.currentPage - 1)] !== 'undefined'){
			this.currentPage--;
			this.showResultsPage(this.currentPage);
		}
		break;
	}
};
jQuery( document ).ready( function( $ ) {
//	console.log('cpt_posts',customsearchcpt_posts);
//	console.log('taxonomy info',customsearchcpt_taxonomy);
//	console.log('config',customsearchcpt_config);
//	console.log('customsearchcpt_metadata',customsearchcpt_metadata);
	var customSearchWidget = new wpCustomSearchCPT();
	
	var jqueryHTML = customSearchWidget.$.fn.html;
	customSearchWidget.$.fn.html = function (html,cb) {
		var ret = jqueryHTML.apply(this,arguments);
		console.log('cb',cb);
		if(typeof cb === 'function'){
			cb();
		}
	    return ret;
	};
	
	//init search widget
	customSearchWidget.init();
	customSearchWidget.resetSearchForm();
	
	$('.customsearch-cpt-container').on('click', '.searchcpt-dropdown-option', function (event) {
		event.stopPropagation();
		var data = $(this).data();
		$(".customsearch-cpt-container [data-taxonomy='"+data.taxonomy+"']").find('.dd-value').text(data.taxvalue);
		$(".customsearch-cpt-container [data-taxonomy='"+data.taxonomy+"'].active").removeClass('active').addClass('selected');
		customSearchWidget.addNextFilter();
		//disable text box
		$('.customsearch-cpt-container .search-text-box').val('').attr('disabled',true);

	});
	$('.customsearch-cpt-container').on('click', '.wrapper-dropdown-searchcpt.noselection', function (event) {
		$(".customsearch-cpt-container .wrapper-dropdown-searchcpt").removeClass('active');
		$(this).removeClass('noselection');
		$(this).toggleClass('active');
		event.stopPropagation();
	});
	$('.customsearch-cpt-container').on('click', '.custom-search-pagination a', function (event) {
		var gotovalue = String($(this).data('gotopage'));
		if(gotovalue == "-1"){
			if(customSearchWidget.currentPage > 1){
				gotovalue = parseInt(customSearchWidget.currentPage,10) - 1;
			}else{
				gotovalue = 1;
			}
		}else if(gotovalue == "+1"){
			if(customSearchWidget.currentPage < customSearchWidget.pagedResults.length){
				gotovalue = parseInt(customSearchWidget.currentPage,10) + 1;
			}else{
				gotovalue = customSearchWidget.pagedResults.length;
			}
		}
		customSearchWidget.currentPage = parseInt(gotovalue,10);
		customSearchWidget.showResultsPage(customSearchWidget.currentPage);
	});
	$('.customsearch-cpt-container').on('click', '#searchcpt-reset-btn', function (event) {
		customSearchWidget.resetSearchForm();
	});
	$('.customsearch-cpt-container').on('click', '#searchcpt-btn,.searchicon-img', function (event) {
		//lock the form
		if(!$(this).hasClass('disabled')){
			$('.customsearch-cpt-container .noselection').removeClass('noselection');
			$('.customsearch-cpt-container img.searchicon-img').removeClass('disabled').addClass('disabled');
			$('.customsearch-cpt-container .search-text-box').removeAttr('disabled').attr('disabled','true');
			customSearchWidget.filterResults();
		}
		
	});
	
//	//custom-search-pagination
	$(document).click(function() {
		// all dropdowns
		$('.customsearch-cpt-container .wrapper-dropdown-searchcpt.active')
			.removeClass('active').removeClass('active')
			.addClass('noselection');
	});
	
    //LOGIN JavaScript
    var searchtype = String( jQuery(".customsearch-cpt-container").data("searchtype") ).toUpperCase();
    var isSupport = false;
    console.log("SEARCHTYPE",searchtype);
    if($('[data-is_support]').data('is_support') == "yes"){
        isSupport = true;
    }
    var loginSetup = function(taxonomy){
        console.log('setting up login for',taxonomy);
        $('#searchcpt-btn').attr('style','display:none;');
        $('#landing-page-login').remove();
        
        var html2add = '<div>';
        html2add += '<button id="landing-page-login" type="button" style="min-width:125px;float: left; padding: 1.15em; background-color: #BC224D; cursor: pointer; font-family: "soleil",Helvetica,Roboto,Arial,sans-serif; font-weight: 300; font-size: 19px; line-height: 1; color: #fff;">Log In</button></div>';
        
        $('.search-ctrl-wrapper').prepend(html2add);
        
        
        $('body').on('click', '#landing-page-login', function(){
            var href = $('[data-taxonomy~="'+taxonomy+'"] .searchcpt-dropdown-option').not('.noshow').attr('value');
            console.log('This is ' + href);
            
            
            if(String(href).indexOf('bencor.rprgonline.com') !== -1){
                var answer = confirm("Note: You are now leaving bencor.usretirementpartners.com and being directed to bencor.rprgonline.com.");
                if (answer){
                    window.location=href;
                    return true;
                }else{
                    return false;
                }    
            }else{
                window.location=href;
                return true;
            }
            
        });
    };
    
    switch(searchtype){
        case "ADVISOR_PORTAL":
            loginSetup('advisor_portal');
            break;
        case "ADVISOR_LOGIN":
            loginSetup('advisor_login');
            break;
    }
} );


