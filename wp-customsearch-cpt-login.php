<?php
/*
 * Plugin Name: CustomSearchCPT-BENCOR-LOGIN
 * Version: 3.0
 * Description: The CustomSearchCPT Wordpress Plugin creates a custom Search & Filtering shortcode for ANY post type.
 * Author: OBLIVIO LLC
 * Author URI: https://www.obliviocompany.com/
 * Requires at least: 4.0
 * Tested up to: 4.0
 *
 * Text Domain: wp-customsearch-cpt-login
 *
 * @package WordPress
 * @author OBLIVIO LLC
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;


function customsearchcpt_login_func( $atts,$content=null ) {
	
	extract( shortcode_atts( array(
			'cpt' => '',
            'search_type' => '',
            'is_support' => '',
			'meta_keys'=>'',
			'meta_key_labels'=>'',
			'taxonomy_keys'=>'',
			'taxonomy_key_labels'=>'',
			'search_caption'=>'',
			'search_button_text'=>'Search',
			'searchfilters_heading'=>'',
			'search_results_heading'=>'Search Results'
		), $atts ) );
	
    //Configuration
	if(strtoupper($search_type) == "ADVISOR_PORTAL") {
		require __DIR__ . '/config/config-advisor_portal.php';
	}else if(strtoupper($search_type) == "ADVISOR_LOGIN") {
		require __DIR__ . '/config/config-advisor_login.php';
	}else {
		require __DIR__ . '/config/config-default.php';
	}
    
	$shortcodeHTML = '';


	if(!$cpt || !$meta_keys ){
		$shortcodeHTML = "CustomSearchCPT - No arguments passed. ";
	}else{
		$args = array(
				//'orderby' => 'ID',
				'numberposts'=>'-1', //returns all
				'post_status' => 'publish',
				'post_type' => $cpt,
		);
		
		$cptentries = get_posts( $args );
		$true_posts = array();
		if(count($cptentries) > 0){
			$shortcodeHTML .= '<div class="customsearch-cpt-container" data-searchtype="'.$search_type.'" data-is_support="'.$is_support.'" >';
			
			//caption
			$shortcodeHTML .= '<h3 class="search-caption">'.$search_caption.'</h3>';
			
			$shortcodeHTML .= '<p class="search-heading">'.$searchfilters_heading.'</p>
			<div class="search-filters-wrapper">	
				<img id="hidden-arrow" src="'.plugin_dir_url( __FILE__ ).'assets/img/drop-down-arrow.png" />	
				<div class="search-wrapper">
					<input class="search-text-box" type="text" value="" placeholder="Enter Advisor Name Here..." />
					<img class="searchicon-img" src="'.plugin_dir_url( __FILE__ ).'assets/img/search-icon.png" />
				</div>
				<div style="clear: both;"></div>
				<p class="customsearchcpt-divider">-OR-</p>
				<div class="filter-wrapper"></div>
				<div style="clear: both;"></div>
				<div class="search-ctrl-wrapper">
				<div id="searchcpt-btn"><button "><img src="'.plugin_dir_url( __FILE__ ).'assets/img/search-icon.png" /> '.$search_button_text.'</button></div>
				<div id="searchcpt-reset-btn"><button>Reset</button></div>
				</div>
				
			</div>
			';
			
			$shortcodeHTML .= "<div id='customsearch-results-container'>";
			
			$shortcodeHTML .= "<p class='searchresults-heading'>".$search_results_heading."</p>";	
            if($is_support == "yes"){
                
                $shortcodeHTML .= '<hr /><hr /><div style="clear:both;"><div class="columns medium-5">\
                            <h3>BENCOR</h3>\
                            <p><strong>Participant&nbsp;Support:</strong>&nbsp;(888) 258-3422</p>\
                            </div></div>';   
            }
			$shortcodeHTML .= '<table id="customsearchcpt-table" class="noshow">';
			
			$metadata_labels = explode(",",$meta_key_labels);
			$metadata_keys = explode(",",$meta_keys);
			$taxdata_labels = explode(",",$taxonomy_key_labels);
			$taxdata_keys = explode(",",$taxonomy_keys);
			
			$shortcodeHTML .= "<thead>";
			$shortcodeHTML .= "<tr>";
			$meta_data = array();
			foreach($metadata_labels as $k=>$tmpMetaLabel){
				$meta_data[] = $metadata_keys[$k];
				$shortcodeHTML .= '<td>'.$tmpMetaLabel.'</td>';
			}
			$shortcodeHTML .= "</tr>";
			$shortcodeHTML .= "</thead>";
			
			$jsTaxonomy = array();
			
			foreach($cptentries as $i=>$cptentry){
				$tmpPost = array();
				$tmpPost['post_id'] = $cptentry->ID;
				foreach($metadata_keys as $k=>$tmpMetaKey){
					$tmpMeta = get_post_meta($cptentry->ID,$tmpMetaKey);
					if(!ISSET($tmpMeta[0])){
						continue;
					}else{
						$tmpPost[$tmpMetaKey] = addslashes(str_replace("'","",$tmpMeta[0]));	
					}
				}
				foreach($taxdata_keys as $k=>$tmpTaxKey){
					$tmpTax = wp_get_post_terms($cptentry->ID,$tmpTaxKey);
					$tmpPost[$tmpTaxKey] = addslashes(str_replace("'","",$tmpTax[0]->name));
				}
				foreach($taxdata_keys as $k=>$tmpTaxKey){
					$tmpTax = wp_get_post_terms($cptentry->ID,$tmpTaxKey);	
					//populate Taxonomy dropdown info
					if(!isset($jsTaxonomy[$taxdata_labels[$k]])){
						$jsTaxonomy[$taxdata_labels[$k]] = array();
						$jsTaxonomy[$taxdata_labels[$k]]['tax_key'] = $tmpTaxKey;
						$jsTaxonomy[$taxdata_labels[$k]]['tax_label'] = $taxdata_labels[$k];
						$jsTaxonomy[$taxdata_labels[$k]]['tax_values'] = array();
						
					}
					
					if(!in_array($tmpPost[$tmpTaxKey],$jsTaxonomy[$taxdata_labels[$k]]['tax_values'])){
						$jsTaxonomy[$taxdata_labels[$k]]['tax_values'][] = $tmpPost[$tmpTaxKey];
						
					}
					
				}
				//single quotes have to be eliminated in order to produce valid JSON
				//As far as I know, JSON.parse(str) only adheres to the official JSON specification and does not accept single quotes.
				$true_posts[] = $tmpPost;
			}
			$shortcodeHTML .= "<tbody id='filtered-search-results'>";
			
			$meta_data = array();
			foreach($true_posts as $j=>$tempPost){
				$shortcodeHTML .= '<tr class="customsearchcpt-tbl-row" id="customsearchcpt-table-row-'.$tempPost['post_id'].'" style="display:none;">';
				foreach($metadata_labels as $k=>$tmpMetaLabel){
					$mKey = $metadata_keys[$k];
					if($mKey == 'email'){
						if(ISSET($tempPost[$mKey]) && $tempPost[$mKey] != ''){
							$shortcodeHTML .= '<td><a href="mailto:'.stripslashes($tempPost[$mKey]).'">'.stripslashes($tempPost[$mKey]).'</a></td>';
						}else{
							$shortcodeHTML .= '<td class="blank-row">'."&nbsp;".'</td>';
						}	
					}else{
						if(ISSET($tempPost[$mKey]) && $tempPost[$mKey] != '' ){
							$shortcodeHTML .= '<td>'.stripslashes($tempPost[$mKey]).'</td>';		
						}else{
							$shortcodeHTML .= '<td class="blank-row">'."&nbsp;".'</td>';
						}
					}
				}
				$shortcodeHTML .= "</tr>";
			}
			$shortcodeHTML .= "</tbody>";
			$shortcodeHTML .= '</table>';
			
			$shortcodeHTML .= "<div id='search-pagination-wrapper'>";
			$shortcodeHTML .= ' <ul class="custom-search-pagination">
  <li><a class="previous-page" data-gotopage="-1">&laquo; previous</a></li></ul>
 <ul class="custom-search-pagination pages">
</ul>
			<ul class="custom-search-pagination"><li><a class="next-page" data-gotopage="+1">next &raquo;</a></li></ul>';
			$shortcodeHTML .= "</div>";
			$shortcodeHTML .= "<p id='empty-results-msg'>&nbsp;</p>";
			$shortcodeHTML .= '</div>'; //end results div
			$shortcodeHTML .= '</div>';
			$shortcodeHTML .= "<script>var customsearchcpt_metadata_raw='".json_encode($metadata_keys)."'; var customsearchcpt_metadata=JSON.parse(customsearchcpt_metadata_raw);</script>";	
			$shortcodeHTML .= "<script>var customsearchcpt_posts_raw='".json_encode($true_posts)."'; var customsearchcpt_posts=JSON.parse(customsearchcpt_posts_raw);</script>";
			$shortcodeHTML .= "<script>var customsearchcpt_taxonomy_raw='".json_encode($jsTaxonomy)."'; var customsearchcpt_taxonomy=JSON.parse(customsearchcpt_taxonomy_raw);</script>";
			$shortcodeHTML .= "<script>var customsearchcpt_config_raw='".json_encode($config)."'; var customsearchcpt_config=JSON.parse(customsearchcpt_config_raw);</script>";
				
		}//end if
	}//end else
		
	//register styles and scripts
	wp_register_script('customsearch_login_script', plugins_url('assets/js/wp-customsearch-cpt.min.js', __FILE__), array('jquery'),'6.5.0', true);
	wp_register_style('customsearch_login_style', plugins_url('assets/css/wp-customsearch-cpt.css', __FILE__), false, '4.0.0');
	
    
    
	wp_enqueue_style('customsearch_login_style');
	wp_enqueue_script('customsearch_login_script');
	
	//return shortcodeHTML
	return $shortcodeHTML;
}
add_shortcode( 'customsearchcpt_login', 'customsearchcpt_login_func' );


