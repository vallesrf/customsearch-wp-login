<?php
return array(
	'Filtering'=>array(
		'1'=>array('tax_key'=>'advisor_state'),
		'2'=>array('tax_key'=>'advisor_city',
				'condition'=>'advisor_state==FL',
				'replace_key'=>'advisor_county'),
		'3'=>array('tax_key'=>'advisor_county'),
		'4'=>array('tax_key'=>'advisor_employer'),
	),
	'Pagination'=>array(
		'posts_per_page'=>'5',
		'empty_results_msg'=>'There is no Advisor that meets your search criteria. Please <a href=\"contact-us/\">contact us</a>.'	
	),
);